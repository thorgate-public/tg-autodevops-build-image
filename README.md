# Thorgate AutoDevOps Build stage image

Thorgate flavored AutoDevOps build stage image

Supports:
- Multi container builds

Example of Dockerfile discovery for `component1`
- `Dockerfile-component1.production`
- `Dockerfile-component1`
- `Dockerfile`
- Herokuish build packs

## Usage
```yaml
variables:
  AUTO_DEVOPS_COMPONENTS: "component1 component2"

build:
  stage: build
  image: "registry.gitlab.com/thorgate-public/tg-autodevops-build-image/master:latest"
  services:
    - docker:stable-dind
  script:
    - apk add -U openssl curl tar gzip bash ca-certificates git
    - |
      if [[ -z "$CI_COMMIT_TAG" ]]; then
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA}
      else
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_TAG}
      fi
    - /build/build.sh
  only:
  - branches
  - tags
```
