#!/bin/bash -e

# build stage script for Thorgate flavored Auto-DevOps

if ! docker info &>/dev/null; then
  if [ -z "$DOCKER_HOST" ] && [ "$KUBERNETES_PORT" ]; then
    export DOCKER_HOST='tcp://localhost:2375'
  fi
fi

if [[ -n "$CI_REGISTRY" && -n "$CI_REGISTRY_USER" ]]; then
  echo "Logging to GitLab Container Registry with CI credentials..."
  docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
fi

enable_cache_from=${AUTO_DEVOPS_ENABLE_BUILD_CACHE_FROM:0}

if [[ -n "$AUTO_DEVOPS_COMPONENTS" ]]; then
  DIR=${AUTO_DEVOPS_DOCKER_FILES_DIR-"."}
  for component in ${AUTO_DEVOPS_COMPONENTS}; do
    component_file="${DIR}/Dockerfile-${component}"
    dockerfile=$component_file

    if [[ -f "${dockerfile}.production" ]]; then
      dockerfile="${dockerfile}.production"
      echo "Building Dockerfile-based application from ${dockerfile}..."
    elif [[ -f "${dockerfile}" ]]; then
      dockerfile="${dockerfile}"
      echo "Building Dockerfile-based application from ${dockerfile}..."
    elif [[ -f Dockerfile ]]; then
      dockerfile="Dockerfile"
      echo "Building Dockerfile-based application from ${dockerfile}..."
    else
      echo "Building Heroku-based application using gliderlabs/herokuish docker image..."
      cp /build/Dockerfile Dockerfile
      dockerfile="Dockerfile"
    fi

    CACHE_FROM_ARGS=""
    if [[ $enable_cache_from -gt 0 ]]; then
      CACHE_FROM_ARGS="--cache-from $CI_APPLICATION_REPOSITORY/${component}:latest"
    fi

    # shellcheck disable=SC2154 # missing variable warning for the lowercase variables
    # shellcheck disable=SC2086 # double quoting for globbing warning for $AUTO_DEVOPS_BUILD_IMAGE_EXTRA_ARGS
    docker build \
      --build-arg BUILDPACK_URL="$BUILDPACK_URL" \
      --build-arg HTTP_PROXY="$HTTP_PROXY" \
      --build-arg http_proxy="$http_proxy" \
      --build-arg HTTPS_PROXY="$HTTPS_PROXY" \
      --build-arg https_proxy="$https_proxy" \
      --build-arg FTP_PROXY="$FTP_PROXY" \
      --build-arg ftp_proxy="$ftp_proxy" \
      --build-arg NO_PROXY="$NO_PROXY" \
      --build-arg no_proxy="$no_proxy" \
      --build-arg COMMIT_HASH="$CI_COMMIT_SHA" \
      $AUTO_DEVOPS_BUILD_IMAGE_EXTRA_ARGS \
      $CACHE_FROM_ARGS \
      -f ${dockerfile} \
      --tag "$CI_APPLICATION_REPOSITORY/${component}:$CI_APPLICATION_TAG" \
      --tag "$CI_APPLICATION_REPOSITORY/${component}:latest" \
      .  # Project root directory

    docker push "$CI_APPLICATION_REPOSITORY/${component}:$CI_APPLICATION_TAG"
    docker push "$CI_APPLICATION_REPOSITORY/${component}:latest"
  done
else
  echo "Variable AUTO_DEVOPS_COMPONENTS was not defined but is required ..."
  exit 1
fi
